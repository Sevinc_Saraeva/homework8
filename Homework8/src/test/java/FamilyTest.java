import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    Family family;
    List <Human> children;
    Human child2;
    Human child1;
    Human testChild;

    @BeforeEach
    public void init() {
        family = new Family(new Man("A", "AA", 1959),
                new Woman("B", "BB", 1959));
        child1 = new Man("C", "AA", 2000);
        child2 = new Woman("D", "AA", 2000);
        children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        family.setChildren(children);
        testChild = new Man("Z", "ZZ", 2000);
    }

    @Test
    public void deleteChildWithIndex(){
        assertTrue(family.deleteChild(1));

    }
    @Test
    public void deleteChildWithWrongIndex(){
        assertFalse(family.deleteChild(-1));

    }

    @Test
    public void  deleteChildWithObject(){
        assertTrue(family.deleteChild(child1));
    }

    @Test
    public void  deleteChildWithWrongObject(){
        assertFalse(family.deleteChild(testChild));
    }
    @Test
    public void  addChild(){
        // before adding child array's length
        int x = family.getChildren().size();
        // adding child
        family.addChild(testChild);
        // after adding child array's length must be equals to x++;
        x++;
        // check condition
        assertTrue(x==family.getChildren().size() &&
                family.getChildren().get(family.getChildren().size()-1).equals(testChild));

    }
    @Test
    public void countFamily(){
        int expected = 2+family.getChildren().size();
        assertEquals(expected, family.countFamily());
    }

}
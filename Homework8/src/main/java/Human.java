import java.util.Arrays;
import java.util.Map;
import java.util.Random;


public abstract class Human {
    private String name;
    private String surname;
    private int year;
    private int iq_level;
    private Family family;
    private Map <String, String> shedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }


    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq_level() {
        return iq_level;
    }

    public void setIq_level(int iq_level) {
        this.iq_level = iq_level;
    }


    public Map<String, String> getShedule() {
        return shedule;
    }

    public void setShedule(String[][] shedule) {
        shedule = shedule;
    }
    public void greetPet(){
        System.out.println("Hello, " + this.family.getPet().get(0).getNickname());

    }

    public Human(String name, String surname, int year, int iq_level, Map<String, String> shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq_level = iq_level;

        this.shedule = shedule;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq_level=" + iq_level +
                ", schedule= " +shedule.toString() +
                '}';
    }



    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Human class");
        super.finalize();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Human))
            return false;
        if (obj == this)
            return true;
        return this.getName() == ((Human) obj).getName() && this.getSurname() == ((Human) obj).getSurname();
    }
}
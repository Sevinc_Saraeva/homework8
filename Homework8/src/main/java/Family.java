import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private List< Pet > pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        children = new ArrayList<>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public List<Pet> getPet() {
        return pet;
    }

    public void setPet(List<Pet> pet) {
        this.pet = pet;
    }

    public boolean addChild(Human child){
       return children.add(child);
    }



    public int countFamily (){
        return this.children.size()+2;
    }


    public boolean equals(Family family) {
        if (this == family) return true;

        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }




    public boolean deleteChild(int n) {
        if (children.size() != 0 && n!=-1) {
          if (n < children.size()) {
             children.remove(n);
             return true;
            } else {
                System.out.println("No child at this index");
                return false;
            }
        }
            return false;
    }

    public boolean deleteChild(Human child) {
       if(children.contains( (child))){
         return  children.remove(child);
       }
        return false;
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + children.hashCode();
        return result;
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Family class");
        super.finalize();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Family))
            return false;
        if (obj == this)
            return true;
        return this.getMother().getName() == ((Family) obj).getMother().getName() && this.getFather().getName() == ((Family) obj).getFather().getName();
    }
    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children.toString() +
                '}';
    }

}
